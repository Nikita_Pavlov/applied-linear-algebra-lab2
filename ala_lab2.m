a=2;
b=3;
c=4;
d=5;

P = [
    -1, -1;
    -1, 1;
    1, 1;
    1, -1;
    ]';


%% 1 Отражение (симметрию) плоскости относительно прямой y = ax
% 
% M1 = [
%     1/sqrt(1+a^2), a/sqrt(1+a^2);
%     -a/sqrt(1+a^2), 1/sqrt(1+a^2);
%     ];
% 
% M2 = [
%     1, 0;
%     0, -1;
%     ];
% 
% M = inv(M1)*M2*M1;
% 
% dojob(M,P,1)
% fplot(@(x) a*x,xlim)
% legend('before','after','eigenvector 1','eigenvector 2','y = ax')
% hold off
% 
% m_analyse(M)
%
% operator:
%    -0.6000    0.8000
%     0.8000    0.6000
% 
% range:
%    -0.6000    0.8000
%     0.8000    0.6000
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%     -1
%      1
% 
% eigenvectors:
%    -0.8944   -0.4472
%     0.4472   -0.8944
% 
% determinant:
%     -1


%% 2 Отображение всей плоскости в прямую y = bx
% 
% M1 = [
%     1/sqrt(1+b^2), b/sqrt(1+b^2);
%     -b/sqrt(1+b^2), 1/sqrt(1+b^2);
%     ];
% 
% M2 = [
%     1, 0;
%     0, 0;
%     ];
% 
% M = inv(M1)*M2*M1;
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
%
% operator:
%     0.1000    0.3000
%     0.3000    0.9000
% 
% range:
%    -0.3162
%    -0.9487
% 
% nullspace:
%    -0.9487
%     0.3162
% 
% eigenvalues:
%          0
%     1.0000
% 
% eigenvectors:
%    -0.9487    0.3162
%     0.3162    0.9487
% 
% determinant:
%      0


%% 3 Поворот плоскости на 10c градусов против часовой стрелки.
% 
% M = [
%     cos(10*c), -sin(10*c);
%     sin(10*c),cos(10*c);
%     ];
% 
% legend('before','after','eigenvector 1','eigenvector 2','y = ax')
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
%
% operator:
%    -0.6669   -0.7451
%     0.7451   -0.6669
% 
% range:
%    -0.6669    0.7451
%     0.7451    0.6669
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%   -0.6669 + 0.7451i
%   -0.6669 - 0.7451i
% 
% eigenvectors:
%    0.7071 + 0.0000i   0.7071 + 0.0000i
%    0.0000 - 0.7071i   0.0000 + 0.7071i
% 
% determinant:
%      1

%% 4 Центральную симметрию плоскости относительно начала координат.
% 
% M = [
%     -1, 0;
%     0, -1;
%     ];
% 
% dojob(M,P+[1.5;-0.5],0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     -1     0
%      0    -1
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%     -1
%     -1
% 
% eigenvectors:
%      1     0
%      0     1
% 
% determinant:
%      1


%% 5 Отображение, которое можно описать так:
% сначала отражение относительно прямой y = ax,
% потом поворот на 10d градусов по часовой стрелке.
% 
% M1 = [
%     1/sqrt(1+a^2), a/sqrt(1+a^2);
%     -a/sqrt(1+a^2), 1/sqrt(1+a^2);
%     ];
% 
% M2 = [
%     1, 0;
%     0, -1;
%     ];
% 
% M3 = [
%     cos(10*d), sin(10*d);
%     -sin(10*d),cos(10*d);
%     ];
% 
% M = M3*inv(M1)*M2*M1;
% 
% dojob(M,P,0)
% fplot(@(x) a*x,xlim)
% legend('before','after','y = ax')
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%    -0.7889    0.6145
%     0.6145    0.7889
% 
% range:
%    -0.7889    0.6145
%     0.6145    0.7889
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    -1.0000
%     1.0000
% 
% eigenvectors:
%    -0.9457   -0.3249
%     0.3249   -0.9457
% 
% determinant:
%    -1.0000


%% 6 Отображение, которое
% переводит прямую y = 0 в y = ax и прямую x = 0 в y = bx
% 
% M = [1, 1;
%     b, a];
% 
% dojob(M,P+[1;1],0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%      1     1
%      3     2
% 
% range:
%    -0.3606   -0.9327
%    -0.9327    0.3606
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    -0.3028
%     3.3028
% 
% eigenvectors:
%    -0.6089   -0.3983
%     0.7933   -0.9172
% 
% determinant:
%     -1


%% 7 Отображение, которое переводит прямую y = ax в y = 0 и прямую y = bx в x = 0
% 
% M = [1, 1;
%      b, a];
% M=inv(M);
% 
% P = [
%     0, 0;
%     1, a;
%     2, 2*b;
%     ]';
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%    -2.0000    1.0000
%     3.0000   -1.0000
% 
% range:
%    -0.5760    0.8174
%     0.8174    0.5760
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    -3.3028
%     0.3028
% 
% eigenvectors:
%    -0.6089   -0.3983
%     0.7933   -0.9172
% 
% determinant:
%    -1.0000


%% 8 Отображение, которое меняет местами прямые y = ax и y = bx.
% 
% M2 = [1, 1;
%      b, a];
% 
% M1 = [1, 1;
%      a, b];
% 
% M=M1*inv(M2);
% 
% P = [
%     0, 0;
%     1, a;
%     2, 2*b;
%     ]';
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.0000         0
%     5.0000   -1.0000
% 
% range:
%    -0.1891   -0.9820
%    -0.9820    0.1891
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    -1.0000
%     1.0000
% 
% eigenvectors:
%          0    0.3714
%     1.0000    0.9285
% 
% determinant:
%    -1.0000


%% 9 Отображение, которое переводит круг единичной площади
% с центром в начале координат в круг площади c
% 
% M = [sqrt(c), 0;
%     0, sqrt(c)];
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%      2     0
%      0     2
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      2
%      2
% 
% eigenvectors:
%      1     0
%      0     1
% 
% determinant:
%      4


%% 10 Отображение, которое переводит круг единичной площади
% с центром в начале координат в некруг площади d
% 
% M = [sqrt(2*c), 0;
%     0, sqrt(c/2)];
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     2.8284         0
%          0    1.4142
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%     1.4142
%     2.8284
% 
% eigenvectors:
%      0     1
%      1     0
% 
% determinant:
%     4.0000


%% 11 Отображение, у которого собственные вектора перпендикулярны,
% и ни один из них не лежит на прямой y = 0 или y = x.
% 
% V = [1, -1;
%      1, 1];
% D= diag([2,1]);
% 
% M = V*D*inv(V);
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.5000    0.5000
%     0.5000    1.5000
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      1
%      2
% 
% eigenvectors:
%    -0.7071    0.7071
%     0.7071    0.7071
% 
% determinant:
%      2


%% 12 Отображение, у которого нет двух неколлинеарных собственных векторов.
% 
% M = [
%     2, 1;
%     0, 2;
%     ];
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%      2     1
%      0     2
% 
% range:
%     0.7882   -0.6154
%     0.6154    0.7882
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      2
%      2
% 
% eigenvectors:
%     1.0000   -1.0000
%          0    0.0000
% 
% determinant:
%      4


%% 13 Отображение, у которого нет ни одного вещественного
% 
% собственного вектора (но при этом само отображение
% задаётся вещественной матрицей).
% 
% M = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% dojob(M,P,0)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     0.7071   -0.7071
%     0.7071    0.7071
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    0.7071 + 0.7071i
%    0.7071 - 0.7071i
% 
% eigenvectors:
%    0.0000 - 0.7071i   0.0000 + 0.7071i
%   -0.7071 + 0.0000i  -0.7071 + 0.0000i
% 
% determinant:
%     1.0000


%% 14 Отображение, для которого любой ненулевой вектор является собственным.
% M = eye(2);
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%      1     0
%      0     1
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      1
%      1
% 
% eigenvectors:
%      1     0
%      0     1
% 
% determinant:
%      1


%% 15.1 Пару отображений, последовательное применение которых
% даёт различные результаты в зависимости от порядка: AB ̸= BA
% 
% A = [2, 0;
%      0, 1];
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=A*B;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.4142   -1.4142
%     0.7071    0.7071
% 
% range:
%    -1.0000    0.0000
%     0.0000    1.0000
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    1.0607 + 0.9354i
%    1.0607 - 0.9354i
% 
% eigenvectors:
%    0.8165 + 0.0000i   0.8165 + 0.0000i
%    0.2041 - 0.5401i   0.2041 + 0.5401i
% 
% determinant:
%     2.0000


%% 15.2 Пару отображений, последовательное применение которых
% даёт различные результаты в зависимости от порядка: AB ̸= BA
% 
% A = [2, 0;
%      0, 1];
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=B*A;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.4142   -0.7071
%     1.4142    0.7071
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    1.0607 + 0.9354i
%    1.0607 - 0.9354i
% 
% eigenvectors:
%    0.2041 + 0.5401i   0.2041 - 0.5401i
%    0.8165 + 0.0000i   0.8165 + 0.0000i
% 
% determinant:
%     2.0000

%% 15.3 Пару отображений, последовательное применение которых
% даёт различные результаты в зависимости от порядка: AB ̸= BA
% отображение A
% 
% A = [2, 0;
%      0, 1];
% 
% M=A;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%      2     0
%      0     1
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      1
%      2
% 
% eigenvectors:
%      0     1
%      1     0
% 
% determinant:
%      2

%% 15.4 Пару отображений, последовательное применение которых
% даёт различные результаты в зависимости от порядка: AB ̸= BA
% отображение B
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=B;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     0.7071   -0.7071
%     0.7071    0.7071
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    0.7071 + 0.7071i
%    0.7071 - 0.7071i
% 
% eigenvectors:
%    0.0000 - 0.7071i   0.0000 + 0.7071i
%   -0.7071 + 0.0000i  -0.7071 + 0.0000i
% 
% determinant:
%     1.0000

%% 16.1 Пару отображений, последовательное применение которых даёт одинаковый
% результат независимо от порядка: AB = BA. Постарайтесь, чтобы матрицы
% A и B были максимально непохожими друг на друга.
% 
% A = [2, 0;
%      0, 2];
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=B*A;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.4142   -1.4142
%     1.4142    1.4142
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    1.4142 + 1.4142i
%    1.4142 - 1.4142i
% 
% eigenvectors:
%    0.7071 + 0.0000i   0.7071 + 0.0000i
%    0.0000 - 0.7071i   0.0000 + 0.7071i
% 
% determinant:
%     4.0000


%% 16.2 Пару отображений, последовательное применение которых даёт одинаковый
% результат независимо от порядка: AB = BA. Постарайтесь, чтобы матрицы
% A и B были максимально непохожими друг на друга.
% 
% A = [2, 0;
%      0, 2];
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=A*B;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     1.4142   -1.4142
%     1.4142    1.4142
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    1.4142 + 1.4142i
%    1.4142 - 1.4142i
% 
% eigenvectors:
%    0.7071 + 0.0000i   0.7071 + 0.0000i
%    0.0000 - 0.7071i   0.0000 + 0.7071i
% 
% determinant:
%     4.0000

%% 16.3 Пару отображений, последовательное применение которых даёт одинаковый
% результат независимо от порядка: AB = BA. Постарайтесь, чтобы матрицы
% 
% A = [2, 0;
%      0, 2];
% 
% M=A;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
%      2     0
%      0     2
% 
% range:
%      1     0
%      0     1
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%      2
%      2
% 
% eigenvectors:
%      1     0
%      0     1
% 
% determinant:
%      4

%% 16.4 Пару отображений, последовательное применение которых даёт одинаковый
% результат независимо от порядка: AB = BA. Постарайтесь, чтобы матрицы
% 
% B = [
%     sqrt(2)/2, -sqrt(2)/2;
%     sqrt(2)/2, sqrt(2)/2;
%     ];
% 
% M=B;
% 
% dojob(M,P,1)
% 
% hold off
% 
% m_analyse(M)
% 
% operator:
%     0.7071   -0.7071
%     0.7071    0.7071
% 
% range:
%    -0.7071   -0.7071
%    -0.7071    0.7071
% 
% nullspace:
%      0
%      0
% 
% eigenvalues:
%    0.7071 + 0.7071i
%    0.7071 - 0.7071i
% 
% eigenvectors:
%    0.0000 - 0.7071i   0.0000 + 0.7071i
%   -0.7071 + 0.0000i  -0.7071 + 0.0000i
% 
% determinant:
%     1.0000


function dojob(M,P,eigvectors)
    clf(gcf)
    P1 = M*P;
    [V,~] = eig(M);
    
    figure(1)
    xlim([-3 3])
    ylim([-3 3])
    axis equal
    hold on

    plot(polyshape(P(1,:),P(2,:)),"EdgeColor",'black','FaceAlpha',0)
    pl=polyshape(P1(1,:),P1(2,:),'Simplify',false);
    plot(pl,"EdgeColor",'red','FaceAlpha',0)

    if eigvectors
        if V(1,1)~=0
            fplot(@(x) V(1,2)/V(1,1)*x,xlim)
        else
            xline(0)
        end

        if V(2,1)~=0
            fplot(@(x) V(2,2)/V(2,1)*x,xlim)
        else
            xline(0)
        end
        legend('before','after','eigenvector 1','eigenvector 2')
    else
        legend('before','after')
    end

    set(gcf,'Position',[1,1,500,500])
end

function m_analyse(M)
    R = orth(M);
    if isempty(R)
        R=[0;0];
    end

    K = null(M);
    if isempty(K)
        K=[0;0];
    end
    
    [V,D] = eig(M);

    d = det(M);

    disp('operator:')
    disp(M)

    disp('range:')
    disp(R)

    disp('nullspace:')
    disp(K)

    disp('eigenvalues:')
    disp(diag(D))

    disp('eigenvectors:')
    disp(V)

    disp('determinant:')
    disp(d)
end